![build-status](https://bitbucket-badges.atlassian.io/badge/atlassianlabs/docker-browse.svg)

# docker-browse #

CLI tool to browse docker registries via [Docker Registry HTTP API V2](https://docs.docker.com/registry/spec/api/)

Requires node 8+.

## Installation ##

```sh
npm install -g docker-browse
```

## Usage ##

```text
  Usage: docker-browse <command> [options]


  Options:

    -V, --version              output the version number
    -r, --registry <registry>  registry to query (index.docker.io)
    -p, --protocol <protocol>  http|https (https)
    -h, --help                 output usage information


  Commands:

    images        output images, paginated
    tags <image>  output tags for <image>
    save          save current options as defaults in ~/.docker-browse.json
```

e.g. list all tags for an image from the default registry:
```text
docker-browse tags library/openjdk
```

e.g. list all tags from an image from a specified registry
```text
docker-browse tags mycool/image -r my.local.registry
```

e.g. list all images from the default registry (will take a LONG time and you might not have access):
```text
docker-browse images
```

e.g. change your default registry:
```text
docker-browse -r my.local.registry save
```

## Authentication and Authorisation ##

Anonymous registries will be directly queried.

Registries that require authentication and authorisaion will read `~/.docker/config.json` for your credentials. See [Docker Registry v2 authentication via central service](https://docs.docker.com/registry/spec/auth/token/) for more details.

If credentials cannot be found, you will be prompted to create them e.g.
```text
FATAL:  cannot find auth for index.docker.io in auths in /somehome/someuser/.docker/config.json
ACTION: docker login index.docker.io
```

## Developing ##

Clone
```text
git clone git@bitbucket.org:atlassianlabs/docker-browse.git
cd docker-browse
```

### system node/npm ###

Install
```text
npm install
```

Execute `bin/docker-browse.js` from the repository root.

### nvm provided node/npm ###

[nvm](https://github.com/creationix/nvm) possibly useful for dev.

Select node/npm version
```text
nvm use
```

Install
```text
npm install
```

Link For Development
```textsh
npm link
```

You may now execute `docker-browse` from anywhere within your nvm managed shell.

## Releasing ##

```text
npm version 3.2.1
npm publish
git push
git push --tags
```

## Contributing ##

Pull requests, issues and comments welcome. For pull requests:

* Separate unrelated changes into multiple pull requests
* branch build must be successful

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

## License ##

Copyright (c) 2017 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.