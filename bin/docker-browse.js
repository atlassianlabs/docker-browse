#!/usr/bin/env node

'use strict';

require('engine-check')();

require('./cli').main();
