'use strict';

const pkg = require('../package.json');
const handleException = require('../lib/exceptions').handleException;
const commands = require('../lib/commands');
const config = require('../lib/config');

const commander = require('commander');

async function cli() {
    const userConfig = await config.read();

    commander
        .version(pkg.version)
        .usage('<command> [options]')
        .option('-r, --registry <registry>', `registry to query (${userConfig.defRegistry})`, userConfig.defRegistry)
        .option('-p, --protocol <protocol>', `http|https (${userConfig.defProtocol})`, /^(http|https)$/,
            userConfig.defProtocol);

    commander
        .command('images')
        .description('output images, paginated')
        .action(() => {
            return handleException(commands.images(commander));
        });

    commander
        .command('tags <image>')
        .description('output tags for <image>')
        .action(() => {
            return handleException(commands.tags(commander));
        });

    commander
        .command('save')
        .description('save current options as defaults in ~/.docker-browse.json')
        .action(() => {
            return handleException(commands.save(commander, userConfig));
        });

    commander.parse(process.argv);

    // ensure that a command was invoked
    if (!(commander.args[commander.args.length - 1] instanceof commander.Command)) {
        commander.help();
    }
}

function main() {
    handleException(cli());
}

module.exports = {
    main
};
