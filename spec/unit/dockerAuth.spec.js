'use strict';

const FatalException = require('../../lib/exceptions').FatalException;

const fs = require('fs');
const path = require('path');
const q = require('q');

const requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();
const jasminePromiseTools = require('jasmine-promise-tools');
const willResolve = jasminePromiseTools.willResolve;
const expectToReject = jasminePromiseTools.expectToReject;

describe('dockerAuth', () => {
    let dockerAuth;

    describe('basicAuth', () => {
        let tmpDir;
        let dockerConfigDirPath;
        let dockerConfigFilePath;

        beforeEach(willResolve(async () => {

            // mock out requires and inject
            const mockOs = jasmine.createSpyObj(['homedir']);
            dockerAuth = requireWithMocks('../../lib/dockerAuth', {
                'os': mockOs
            });

            // create tmpDir containing .docker subdirectory
            tmpDir = await q.nfcall(fs.mkdtemp, '/tmp' + path.sep);
            await q.nfcall(fs.mkdir, tmpDir + path.sep + '.docker');
            dockerConfigDirPath = tmpDir + path.sep + '.docker';

            // mock out os.homedir to return tmpDir
            mockOs.homedir.and.returnValue(tmpDir);

            // expected config.json file
            dockerConfigFilePath = dockerConfigDirPath + path.sep + 'config.json';

        }));

        describe('valid .docker/config.json', () => {

            beforeEach(willResolve(async () => {

                // create valid config.json in dockerConfigDir
                await q.nfcall(fs.writeFile, dockerConfigFilePath, JSON.stringify({
                    auths: {
                        validHost: {
                            auth: 'validAuth'
                        }
                    }
                }));
            }));

            it('should return valid auth', willResolve(() => {
                return dockerAuth.basicAuth('validHost')
                    .then((basicAuth) => {
                        expect(basicAuth).toEqual('validAuth');
                    });
            }));

            it('should thrown a FatalException on missing host', willResolve(() => {
                return expectToReject(dockerAuth.basicAuth('wtfHost'))
                    .then((err) => {
                        expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                        expect(err.message).toEqual(`cannot find auth for wtfHost in auths in ${dockerConfigFilePath}`);
                        expect(err.rc).toEqual(15);
                        expect(err.action).toEqual('docker login wtfHost');
                    });
            }));

            afterEach(willResolve(async () => {

                // remove config.json
                await q.nfcall(fs.unlink, dockerConfigFilePath);
            }));
        });

        describe('missing .docker/config.json', () => {

            it('should throw a FatalException', willResolve(() => {
                return expectToReject(dockerAuth.basicAuth('ohwow'))
                    .then((err) => {
                        expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                        expect(err.message).toEqual(`cannot find ${dockerConfigFilePath}`);
                        expect(err.rc).toEqual(13);
                        expect(err.action).toEqual('docker login ohwow');
                    });
            }));
        });

        describe('invalid .docker/config.json', () => {

            beforeEach(willResolve(async () => {

                // create dodgey config.json in dockerConfigDir
                await q.nfcall(fs.writeFile, dockerConfigFilePath, 'omgwtf');
            }));

            it('should throw a FatalException', willResolve(() => {
                return expectToReject(dockerAuth.basicAuth('bbq'))
                    .then((err) => {
                        expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                        expect(err.message).toEqual(`cannot parse ${dockerConfigFilePath}`);
                        expect(err.rc).toEqual(14);
                        expect(err.action).toEqual('docker login bbq');
                    });
            }));

            afterEach(willResolve(async () => {

                // remove config.json
                await q.nfcall(fs.unlink, dockerConfigFilePath);
            }));
        });

        describe('unexpected error', () => {

            beforeEach(() => {

                // mock out fs to return a big derp on reading a file
                const mockFs = jasmine.createSpyObj(['readFile']);
                mockFs.readFile.and.throwError('derp');

                dockerAuth = requireWithMocks('../../lib/dockerAuth', {
                    'fs': mockFs
                });
            });

            it('should throw Error', willResolve(() => {
                return expectToReject(dockerAuth.basicAuth('somethingsomething'))
                    .then((err) => {
                        expect(err instanceof Error).toBeTruthy('expected Error');
                        expect(err.message).toEqual('derp');
                    });
            }));
        });

        describe('credentials in OS X keychain', () => {

            beforeEach(willResolve(async () => {

                // create os x like config.json in dockerConfigDir
                await q.nfcall(fs.writeFile, dockerConfigFilePath, JSON.stringify({
                    credsStore: 'osxkeychain'
                }));
            }));

            it('should throw a FatalException', willResolve(() => {
                return expectToReject(dockerAuth.basicAuth('wtfHost'))
                    .then((err) => {
                        expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                        expect(err.message).toEqual(`${dockerConfigFilePath} stores credentials in OS X keychain`);
                        expect(err.rc).toEqual(16);
                        expect(err.action).toEqual(
                            'go to docker preferences and untick "Securely store docker logins in macOS keychain"');
                    });
            }));

            afterEach(willResolve(async () => {

                // remove config.json
                await q.nfcall(fs.unlink, dockerConfigFilePath);
            }));
        });

        afterEach(willResolve(async () => {

            // remove tmpDir
            await q.nfcall(fs.rmdir, dockerConfigDirPath);
            await q.nfcall(fs.rmdir, tmpDir);
        }));
    });
});
