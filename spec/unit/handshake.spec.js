'use strict';

const requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();
const jasminePromiseTools = require('jasmine-promise-tools');
const willResolve = jasminePromiseTools.willResolve;

describe('handshake', () => {
    let handshake;
    let mockRegistry;
    let mockDockerAuth;

    beforeEach(() => {

        // mock out requires and inject
        mockRegistry = jasmine.createSpyObj([
            'requiresAuth',
            'handshakeForApiCheck',
            'authenticateForApiCheck',
            'apiCheck',
            'handshakeForData',
            'authenticateForData'
        ]);
        mockDockerAuth = jasmine.createSpyObj([
            'basicAuth'
        ]);
        handshake = requireWithMocks('../../lib/handshake', {
            './registry': mockRegistry,
            './dockerAuth': mockDockerAuth
        });

        // mock out the common calls
        mockRegistry.handshakeForApiCheck.and.returnValue('apiCheckHeader');
        mockDockerAuth.basicAuth.and.returnValue('dockerBasicAuth');
        mockRegistry.authenticateForApiCheck.and.returnValue('apiCheckToken');
        mockRegistry.handshakeForData.and.returnValue('dataHeader');
        mockRegistry.authenticateForData.and.returnValue('bearerToken');
    });

    it('should handshake upon anonymous fail', willResolve(() => {
        mockRegistry.requiresAuth.and.returnValue(true);

        return handshake.handshake('myprot', 'myreg', 'mypath')
            .then((bearerToken) => {
                expect(bearerToken).toEqual('bearerToken');

                expect(mockRegistry.requiresAuth).toHaveBeenCalledWith('myprot', 'myreg');
                expect(mockRegistry.handshakeForApiCheck).toHaveBeenCalledWith('myprot', 'myreg');
                expect(mockDockerAuth.basicAuth).toHaveBeenCalledWith('myreg');
                expect(mockRegistry.authenticateForApiCheck).toHaveBeenCalledWith('myreg', 'apiCheckHeader',
                    'dockerBasicAuth');
                expect(mockRegistry.apiCheck).toHaveBeenCalledWith('myprot', 'myreg', 'apiCheckToken');
                expect(mockRegistry.handshakeForData).toHaveBeenCalledWith('myprot', 'myreg', 'mypath');
                expect(mockRegistry.authenticateForData).toHaveBeenCalledWith('dataHeader', 'dockerBasicAuth');
            });
    }));

    it('should return null for anonymous', willResolve(() => {
        mockRegistry.requiresAuth.and.returnValue(false);

        return handshake.handshake('myprot', 'myreg', 'mypath')
            .then((bearerToken) => {
                expect(bearerToken).toBeNull();

                expect(mockRegistry.requiresAuth).toHaveBeenCalledWith('myprot', 'myreg');
                expect(mockRegistry.handshakeForApiCheck).toHaveBeenCalledTimes(0);
                expect(mockDockerAuth.basicAuth).toHaveBeenCalledTimes(0);
                expect(mockRegistry.authenticateForApiCheck).toHaveBeenCalledTimes(0);
                expect(mockRegistry.apiCheck).toHaveBeenCalledTimes(0);
                expect(mockRegistry.handshakeForData).toHaveBeenCalledTimes(0);
                expect(mockRegistry.authenticateForData).toHaveBeenCalledTimes(0);
            });
    }));
});