'use strict';

const requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();
const jasminePromiseTools = require('jasmine-promise-tools');
const willResolve = jasminePromiseTools.willResolve;

describe('commands', () => {
    let commands;
    let mockConfig;
    let mockRegistry;
    let mockHandshake;

    beforeEach(() => {

        // mock out requires and inject
        mockConfig = jasmine.createSpyObj([
            'write'
        ]);
        mockRegistry = jasmine.createSpyObj([
            'printData'
        ]);
        mockHandshake = jasmine.createSpyObj([
            'handshake'
        ]);
        commands = requireWithMocks('../../lib/commands', {
            './config': mockConfig,
            './registry': mockRegistry,
            './handshake': mockHandshake
        });

        // capture console.log
        spyOn(console, 'log');

        // mock out the common calls
        mockHandshake.handshake.and.returnValue('bearerToken');
    });

    describe('save', () => {
        it('should populate and persist', willResolve(() => {
            return commands.save({ registry: 'somereg' }, { what: 'dafuq' })
                .then(() => {
                    expect(mockConfig.write).toHaveBeenCalledWith({ what: 'dafuq', defRegistry: 'somereg' });
                });
        }));
    });

    describe('images', () => {
        it('should handshake and print', willResolve(() => {
            mockRegistry.printData.and.callFake((protocol, host, path, bearerToken, printFn) => {
                printFn({ repositories: ['one', 'two'] });
            });

            return commands.images({ protocol: 'myprot', registry: 'myreg' })
                .then(() => {
                    expect(console.log).toHaveBeenCalledWith('one');
                    expect(console.log).toHaveBeenCalledWith('two');
                });
        }));

        it('should handshake and ignore dodgey data', willResolve(() => {
            mockRegistry.printData.and.callFake((protocol, host, path, bearerToken, printFn) => {
                printFn({ blargh: ['one', 'two'] });
            });

            return commands.images({ protocol: 'myprot', registry: 'myreg' })
                .then(() => {
                    expect(console.log).toHaveBeenCalledTimes(0);
                });
        }));

        afterEach(willResolve(async () => {
            expect(mockHandshake.handshake).toHaveBeenCalledWith('myprot', 'myreg', '/v2/_catalog');
            expect(mockRegistry.printData).toHaveBeenCalledWith('myprot', 'myreg', '/v2/_catalog',
                'bearerToken', jasmine.any(Function));
        }));
    });

    describe('tags', () => {
        it('should handshake and print', willResolve(() => {
            mockRegistry.printData.and.callFake((protocol, host, path, bearerToken, printFn) => {
                printFn({ tags: ['one', 'two'] });
            });

            return commands.tags({ protocol: 'myprot', registry: 'myreg', args: ['myimage'] })
                .then(() => {
                    expect(console.log).toHaveBeenCalledWith('one');
                    expect(console.log).toHaveBeenCalledWith('two');
                });
        }));

        it('should handshake and ignore dodgey data', willResolve(() => {
            mockRegistry.printData.and.callFake((protocol, host, path, bearerToken, printFn) => {
                printFn({ slartibartfast: ['one', 'two'] });
            });

            return commands.tags({ protocol: 'myprot', registry: 'myreg', args: ['myimage'] })
                .then(() => {
                    expect(console.log).toHaveBeenCalledTimes(0);
                });
        }));

        afterEach(willResolve(async () => {
            expect(mockHandshake.handshake).toHaveBeenCalledWith('myprot', 'myreg', '/v2/myimage/tags/list');
            expect(mockRegistry.printData).toHaveBeenCalledWith('myprot', 'myreg', '/v2/myimage/tags/list',
                'bearerToken', jasmine.any(Function));
        }));
    });
});