'use strict';

const FatalException = require('../../lib/exceptions').FatalException;

const requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();
const jasminePromiseTools = require('jasmine-promise-tools');
const willResolve = jasminePromiseTools.willResolve;
const expectToReject = jasminePromiseTools.expectToReject;

const DOCKER_HANDSHAKE_TIMEOUT_MS = 5000; // 5sec
const DOCKER_DATA_TIMEOUT_MS = 60000; // 1min
const DOCKER_DATA_MAXSIZE = 1 * 1024 * 1024; // 1MiB, up from 512KiB default

describe('registry', () => {
    let registry;
    let mockHttplease;

    beforeEach(() => {

        // mock the request builder
        mockHttplease = jasmine.createSpyObj([
            'builder',
            'withMethodGet',
            'withBaseUrl',
            'withTimeout',
            'withHeaders',
            'withParams',
            'withExpectStatus',
            'withBufferJsonResponseHandler',
            'withDiscardBodyResponseHandler',
            'send'
        ]);
        mockHttplease.builder.and.returnValue(mockHttplease);
        mockHttplease.withMethodGet.and.returnValue(mockHttplease);
        mockHttplease.withBaseUrl.and.returnValue(mockHttplease);
        mockHttplease.withTimeout.and.returnValue(mockHttplease);
        mockHttplease.withHeaders.and.returnValue(mockHttplease);
        mockHttplease.withParams.and.returnValue(mockHttplease);
        mockHttplease.withExpectStatus.and.returnValue(mockHttplease);
        mockHttplease.withBufferJsonResponseHandler.and.returnValue(mockHttplease);
        mockHttplease.withDiscardBodyResponseHandler.and.returnValue(mockHttplease);
    });

    describe('requiresAuth', () => {
        let protocol;
        let host;

        beforeEach(() => {

            // reset test vars
            protocol = 'someproto';
            host = 'somehost';

            // inject it
            registry = requireWithMocks('../../lib/registry', {
                'httplease': mockHttplease
            });
        });

        it('should return false on OK', willResolve(() => {
            mockHttplease.send.and.callFake(() => {
                return Promise.resolve();
            });

            return registry.requiresAuth(protocol, host)
                .then((requiresAuth) => {
                    expect(requiresAuth).toEqual(false);
                });
        }));

        it('should return true on any fail except host not found', willResolve(() => {
            const err = new Error();
            mockHttplease.send.and.returnValue(Promise.reject(err));

            return registry.requiresAuth(protocol, host)
                .then((requiresAuth) => {
                    expect(requiresAuth).toEqual(true);
                });
        }));

        it('should throw FatalException on unresolvable host', willResolve(() => {
            const err = new Error();
            err.code = 'ENOTFOUND';
            err.hostname = host;
            mockHttplease.send.and.returnValue(Promise.reject(err));

            return expectToReject(registry.requiresAuth(protocol, host))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual(`host ${host} not found`);
                    expect(err.rc).toEqual(22);
                    expect(err.action).toBeUndefined();
                });
        }));

        afterEach(() => {
            expect(mockHttplease.builder).toHaveBeenCalledWith();
            expect(mockHttplease.withMethodGet).toHaveBeenCalledWith();
            expect(mockHttplease.withBaseUrl).toHaveBeenCalledWith(`${protocol}://${host}/v2/`);
            expect(mockHttplease.withTimeout).toHaveBeenCalledWith(DOCKER_HANDSHAKE_TIMEOUT_MS);
            expect(mockHttplease.withExpectStatus).toHaveBeenCalledWith([200]);
            expect(mockHttplease.withDiscardBodyResponseHandler).toHaveBeenCalledWith();
            expect(mockHttplease.send).toHaveBeenCalled();
        });
    });

    describe('handshakeForApiCheck', () => {
        let headers;
        let protocol;
        let host;
        let statusCode;

        beforeEach(() => {

            // reset test vars
            protocol = 'someproto';
            host = 'somehost';
            headers = {};

            // mock response
            mockHttplease.send.and.callFake(() => {
                return Promise.resolve({
                    'headers': headers,
                    'statusCode': statusCode
                });
            });

            // inject it
            registry = requireWithMocks('../../lib/registry', {
                'httplease': mockHttplease
            });
        });

        it('should return on success', willResolve(() => {

            statusCode = 200;
            headers = { 'www-authenticate': ' Bearer realm = "somerealm" , service = "someservice" ' };

            return registry.handshakeForApiCheck(protocol, host)
                .then((apiCheckHeader) => {
                    expect(apiCheckHeader.bearerRealm).toEqual('somerealm');
                    expect(apiCheckHeader.service).toEqual('someservice');
                });
        }));

        it('should throw FatalException on missing www-authenticate header', willResolve(() => {

            return expectToReject(registry.handshakeForApiCheck(protocol, host))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual(`www-authenticate header not returned from ${host}`);
                    expect(err.rc).toEqual(21);
                    expect(err.action).toBeUndefined();
                });
        }));

        it('should throw FatalException on missing Bearer realm', willResolve(() => {

            statusCode = 200;
            headers = { 'www-authenticate': ' derp = "somerealm" , service = "someservice" ' };

            return expectToReject(registry.handshakeForApiCheck(protocol, host))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual('"Bearer realm" not present in www-authenticate header');
                    expect(err.rc).toEqual(23);
                    expect(err.action).toBeUndefined();
                });
        }));

        it('should throw FatalException on missing service', willResolve(() => {

            statusCode = 200;
            headers = { 'www-authenticate': ' Bearer realm = "somerealm" , derp = "someservice" ' };

            return expectToReject(registry.handshakeForApiCheck(protocol, host))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual('"service" not present in www-authenticate header');
                    expect(err.rc).toEqual(24);
                    expect(err.action).toBeUndefined();
                });
        }));

        it('should throw FatalException on invalid HTTP response', () => {

            statusCode = 404;

            return expectToReject(registry.handshakeForApiCheck(protocol, host))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual(`${host} does not appear to be a valid V2 API docker registry`);
                    expect(err.rc).toEqual(25);
                    expect(err.action).toBeUndefined();
                });
        });

        it('should propagate unexpected exceptions', () => {

            mockHttplease.send.and.returnValue(Promise.reject(new Error('myargh!')));

            return expectToReject(registry.handshakeForApiCheck(protocol, host))
                .then((err) => {
                    expect(err instanceof Error).toBeTruthy('expected Error');
                    expect(err.message).toEqual('myargh!');
                });
        });

        afterEach(() => {
            expect(mockHttplease.builder).toHaveBeenCalledWith();
            expect(mockHttplease.withMethodGet).toHaveBeenCalledWith();
            expect(mockHttplease.withBaseUrl).toHaveBeenCalledWith(`${protocol}://${host}/v2/`);
            expect(mockHttplease.withTimeout).toHaveBeenCalledWith(DOCKER_HANDSHAKE_TIMEOUT_MS);
            expect(mockHttplease.withExpectStatus).toHaveBeenCalledWith([401, 404]);
            expect(mockHttplease.withDiscardBodyResponseHandler).toHaveBeenCalledWith();
            expect(mockHttplease.send).toHaveBeenCalled();
        });
    });

    describe('authenticateForApiCheck', () => {
        let host;
        let handshakeHeader;
        let basicAuth;
        let statusCode;
        let body;

        beforeEach(() => {

            // reset test vars
            host = 'somehost';
            handshakeHeader = {
                bearerRealm: 'somebearerrealm'
            };
            basicAuth = 'somebasicauth';
            statusCode = 0;
            body = {};

            // mock response
            mockHttplease.send.and.callFake(() => {
                return Promise.resolve({
                    'statusCode': statusCode,
                    'body': body
                });
            });

            // inject it
            registry = requireWithMocks('../../lib/registry', {
                'httplease': mockHttplease
            });
        });

        it('should throw FatalException on invalid HTTP response', () => {

            statusCode = 404;

            return expectToReject(registry.authenticateForApiCheck(host, handshakeHeader, basicAuth))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual(`${host} does not appear to be a valid V2 API docker registry`);
                    expect(err.rc).toEqual(31);
                    expect(err.action).toBeUndefined();
                });
        });

        it('should return on success', willResolve(() => {

            statusCode = 200;
            body = {
                token: 'sometoken'
            };

            return registry.authenticateForApiCheck(host, handshakeHeader, basicAuth)
                .then((apiCheckToken) => {
                    expect(apiCheckToken).toEqual('sometoken');
                });
        }));

        it('should throw FatalException on invalid auth', willResolve(() => {

            statusCode = 401;

            return expectToReject(registry.authenticateForApiCheck(host, handshakeHeader, basicAuth))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual('failed authentication');
                    expect(err.rc).toEqual(32);
                    expect(err.action).toEqual(`docker login ${host}`);
                });
        }));

        it('should throw FatalException on invalid auth', willResolve(() => {

            statusCode = 200;

            return expectToReject(registry.authenticateForApiCheck(host, handshakeHeader, basicAuth))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual(`token not returned from ${handshakeHeader.bearerRealm}`);
                    expect(err.rc).toEqual(33);
                    expect(err.action).toBeUndefined();
                });
        }));

        afterEach(() => {
            expect(mockHttplease.builder).toHaveBeenCalledWith();
            expect(mockHttplease.withMethodGet).toHaveBeenCalledWith();
            expect(mockHttplease.withBaseUrl).toHaveBeenCalledWith(handshakeHeader.bearerRealm);
            expect(mockHttplease.withTimeout).toHaveBeenCalledWith(DOCKER_HANDSHAKE_TIMEOUT_MS);
            expect(mockHttplease.withHeaders).toHaveBeenCalledWith({
                Authorization: 'Basic ' + basicAuth
            });
            expect(mockHttplease.withParams).toHaveBeenCalledWith({
                service: handshakeHeader.service
            });
            expect(mockHttplease.withExpectStatus).toHaveBeenCalledWith([200, 401, 404]);
            expect(mockHttplease.withBufferJsonResponseHandler).toHaveBeenCalledWith();
            expect(mockHttplease.send).toHaveBeenCalled();
        });
    });


    describe('apiCheck', () => {
        let protocol;
        let host;
        let bearerToken;
        let statusCode;

        beforeEach(() => {

            // reset test vars
            protocol = 'someproto';
            host = 'somehost';
            bearerToken = 'somebearertoken';

            // mock response
            mockHttplease.send.and.callFake(() => {
                return Promise.resolve({
                    'statusCode': statusCode
                });
            });

            // inject it
            registry = requireWithMocks('../../lib/registry', {
                'httplease': mockHttplease
            });
        });

        it('should do nothing on success', () => {

            statusCode = 200;

            return registry.apiCheck(protocol, host, bearerToken);
        });

        it('should throw FatalException on invalid HTTP response', () => {

            statusCode = 404;

            return expectToReject(registry.apiCheck(protocol, host, bearerToken))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual(`${host} does not appear to be a valid V2 API docker registry`);
                    expect(err.rc).toEqual(41);
                    expect(err.action).toBeUndefined();
                });
        });

        afterEach(() => {
            expect(mockHttplease.builder).toHaveBeenCalledWith();
            expect(mockHttplease.withMethodGet).toHaveBeenCalledWith();
            expect(mockHttplease.withBaseUrl).toHaveBeenCalledWith(`${protocol}://${host}/v2/`);
            expect(mockHttplease.withTimeout).toHaveBeenCalledWith(DOCKER_HANDSHAKE_TIMEOUT_MS);
            expect(mockHttplease.withHeaders).toHaveBeenCalledWith({
                Authorization: 'Bearer ' + bearerToken
            });
            expect(mockHttplease.withExpectStatus).toHaveBeenCalledWith([200, 404]);
            expect(mockHttplease.withDiscardBodyResponseHandler).toHaveBeenCalledWith();
            expect(mockHttplease.send).toHaveBeenCalled();
        });
    });


    describe('handshakeForData', () => {
        let protocol;
        let host;
        let path;
        let headers;
        let statusCode;

        beforeEach(() => {

            // reset test vars
            protocol = 'someproto';
            host = 'somehost';
            path = '/somepath';
            headers = {};
            statusCode = 200;

            // mock response
            mockHttplease.send.and.callFake(() => {
                return Promise.resolve({
                    'headers': headers,
                    'statusCode': statusCode
                });
            });

            // inject it
            registry = requireWithMocks('../../lib/registry', {
                'httplease': mockHttplease
            });
        });

        it('should return on success', willResolve(() => {

            headers = {
                'www-authenticate':
                ' Bearer realm = "somerealm" , service = "someservice" , scope = "somescope" '
            };

            return registry.handshakeForData(protocol, host, path)
                .then((apiCheckHeader) => {
                    expect(apiCheckHeader.bearerRealm).toEqual('somerealm');
                    expect(apiCheckHeader.service).toEqual('someservice');
                    expect(apiCheckHeader.scope).toEqual('somescope');
                });
        }));

        it('should throw FatalException on 404', willResolve(() => {

            statusCode = 404;

            return expectToReject(registry.handshakeForData(protocol, host, path))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual(`https://${host}${path} not found`);
                    expect(err.rc).toEqual(52);
                    expect(err.action).toBeUndefined();
                });
        }));

        it('should throw FatalException on missing www-authenticate header', willResolve(() => {

            return expectToReject(registry.handshakeForData(protocol, host, path))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual(`www-authenticate header not returned from ${host}`);
                    expect(err.rc).toEqual(51);
                    expect(err.action).toBeUndefined();
                });
        }));

        it('should throw FatalException on missing Bearer realm', willResolve(() => {

            headers = {
                'www-authenticate':
                ' derp = "somerealm" , service = "someservice" , scope = "somescope" '
            };

            return expectToReject(registry.handshakeForData(protocol, host, path))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual('"Bearer realm" not present in www-authenticate header');
                    expect(err.rc).toEqual(53);
                    expect(err.action).toBeUndefined();
                });
        }));

        it('should throw FatalException on missing service', willResolve(() => {

            headers = {
                'www-authenticate':
                ' Bearer realm = "somerealm" , derp = "someservice" , scope = "somescope" '
            };

            return expectToReject(registry.handshakeForData(protocol, host, path))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual('"service" not present in www-authenticate header');
                    expect(err.rc).toEqual(54);
                    expect(err.action).toBeUndefined();
                });
        }));

        it('should throw FatalException on missing scope', willResolve(() => {

            headers = {
                'www-authenticate':
                ' Bearer realm = "somerealm" , service = "someservice" , derp = "somescope" '
            };

            return expectToReject(registry.handshakeForData(protocol, host, path))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual('"scope" not present in www-authenticate header');
                    expect(err.rc).toEqual(55);
                    expect(err.action).toBeUndefined();
                });
        }));

        afterEach(() => {
            expect(mockHttplease.builder).toHaveBeenCalledWith();
            expect(mockHttplease.withMethodGet).toHaveBeenCalledWith();
            expect(mockHttplease.withBaseUrl).toHaveBeenCalledWith(`${protocol}://${host}${path}`);
            expect(mockHttplease.withTimeout).toHaveBeenCalledWith(DOCKER_HANDSHAKE_TIMEOUT_MS);
            expect(mockHttplease.withExpectStatus).toHaveBeenCalledWith([401, 404]);
            expect(mockHttplease.withDiscardBodyResponseHandler).toHaveBeenCalledWith();
            expect(mockHttplease.send).toHaveBeenCalled();
        });
    });


    describe('authenticateForData', () => {
        let authHeader;
        let basicAuth;
        let body;
        let statusCode;

        beforeEach(() => {

            // reset test vars
            authHeader = {
                bearerRealm: 'somebearerrealm',
                service: 'someservice',
                scope: 'somescope'
            };
            basicAuth = 'somebasicauth';
            body = {};
            statusCode = 0;

            // mock response
            mockHttplease.send.and.callFake(() => {
                return Promise.resolve({
                    'statusCode': statusCode,
                    'body': body
                });
            });

            // inject it
            registry = requireWithMocks('../../lib/registry', {
                'httplease': mockHttplease
            });
        });

        it('should return on success', willResolve(() => {

            statusCode = 200;
            body = {
                token: 'sometoken'
            };

            return registry.authenticateForData(authHeader, basicAuth)
                .then((token) => {
                    expect(token).toEqual('sometoken');
                });
        }));

        it('should throw FatalException on missing token', willResolve(() => {

            return expectToReject(registry.authenticateForData(authHeader, basicAuth))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual(`token not found in response from ${authHeader.bearerRealm}`);
                    expect(err.rc).toEqual(42);
                    expect(err.action).toBeUndefined();
                });
        }));

        it('should throw FatalException on invalid auth', willResolve(() => {

            statusCode = 401;
            body = {
                details: 'det'
            };

            return expectToReject(registry.authenticateForData(authHeader, basicAuth))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual(`received data auth response from ${authHeader.bearerRealm} "det"`);
                    expect(err.rc).toEqual(41);
                    expect(err.action).toBeUndefined();
                });
        }));

        afterEach(() => {
            expect(mockHttplease.builder).toHaveBeenCalledWith();
            expect(mockHttplease.withMethodGet).toHaveBeenCalledWith();
            expect(mockHttplease.withBaseUrl).toHaveBeenCalledWith(authHeader.bearerRealm);
            expect(mockHttplease.withTimeout).toHaveBeenCalledWith(DOCKER_HANDSHAKE_TIMEOUT_MS);
            expect(mockHttplease.withHeaders).toHaveBeenCalledWith({
                Authorization: 'Basic ' + basicAuth
            });
            expect(mockHttplease.withParams).toHaveBeenCalledWith({
                service: authHeader.service,
                scope: authHeader.scope
            });
            expect(mockHttplease.withExpectStatus).toHaveBeenCalledWith([200, 401]);
            expect(mockHttplease.withBufferJsonResponseHandler).toHaveBeenCalledWith();
            expect(mockHttplease.send).toHaveBeenCalled();
        });
    });


    describe('printData', () => {
        let protocol;
        let host;
        let path;
        let bearerToken;
        let statusCode;
        let body;
        let callHeaders;
        let nextHeaders;
        let printFn;

        beforeEach(() => {

            // reset test vars
            protocol = 'someproto';
            host = 'somehost';
            path = 'somepath';
            bearerToken = 'somebearertoken';
            statusCode = 0;
            body = null;
            callHeaders = {};
            nextHeaders = {};

            // mock response
            mockHttplease.send.and.callFake(() => {
                return Promise.resolve({
                    'statusCode': statusCode,
                    'body': body,
                    'headers': nextHeaders
                });
            });

            // inject it
            registry = requireWithMocks('../../lib/registry', {
                'httplease': mockHttplease
            });
        });

        it('excludes auth header when no bearerToken', willResolve(async () => {

            statusCode = 404;
            callHeaders = {};

            await registry.printData(protocol, host, path, null, null);
        }));

        it('includes auth header from bearerToken', willResolve(async () => {

            statusCode = 404;
            callHeaders = { Authorization: 'Bearer ' + bearerToken };

            await registry.printData(protocol, host, path, bearerToken, null);
        }));

        it('should throw FatalException when not authorised', willResolve(() => {

            statusCode = 401;
            callHeaders = { Authorization: 'Bearer ' + bearerToken };

            return expectToReject(registry.printData(protocol, host, path, bearerToken, printFn))
                .then((err) => {
                    expect(err instanceof FatalException).toBeTruthy('expected FatalException');
                    expect(err.message).toEqual(`not authorised for ${path}`);
                    expect(err.rc).toEqual(71);
                    expect(err.action).toBeUndefined();
                });
        }));

        it('should print until 404 not found', willResolve(async () => {

            statusCode = 200;
            callHeaders = { Authorization: 'Bearer ' + bearerToken };
            nextHeaders['link'] = '<nextlink>; rel="next"';

            let printCount = 0;
            printFn = () => {
                printCount++;
                statusCode = 404;
            };

            await registry.printData(protocol, host, path, bearerToken, printFn);

            expect(printCount).toEqual(1);
        }));

        it('should print until link not given', willResolve(async () => {

            statusCode = 200;
            callHeaders = { Authorization: 'Bearer ' + bearerToken };
            nextHeaders['link'] = '<nextlink>; rel="next"';
            let printCount = 0;

            const printFn = () => {
                printCount++;
                nextHeaders['link'] = null;
            };

            await registry.printData(protocol, host, path, bearerToken, printFn);

            expect(printCount).toEqual(1);
        }));

        it('should print until link does not contain a valid next link', willResolve(async () => {

            statusCode = 200;
            callHeaders = { Authorization: 'Bearer ' + bearerToken };
            nextHeaders['link'] = '<nextlink>; rel="next"';
            let printCount = 0;

            const printFn = () => {
                printCount++;
                if (printCount === 1) {
                    nextHeaders['link'] = '<nextnextlink>; rel="next"';
                } else {
                    nextHeaders['link'] = '<asdf; rel="next"';
                }
            };

            await registry.printData(protocol, host, path, bearerToken, printFn);

            expect(printCount).toEqual(2);
        }));

        it('should print until link does not contain a any next link', willResolve(async () => {

            statusCode = 200;
            callHeaders = { Authorization: 'Bearer ' + bearerToken };
            nextHeaders['link'] = '<nextlink>; rel="next"';
            let printCount = 0;

            const printFn = () => {
                printCount++;
                if (printCount === 1) {
                    nextHeaders['link'] = '<nextnextlink>; rel="next"';
                } else {
                    nextHeaders['link'] = '<nextnextnextlink>; rel="bollocks"';
                }
            };

            await registry.printData(protocol, host, path, bearerToken, printFn);

            expect(printCount).toEqual(2);
        }));

        afterEach(() => {
            expect(mockHttplease.builder).toHaveBeenCalledWith();
            expect(mockHttplease.withMethodGet).toHaveBeenCalledWith();
            expect(mockHttplease.withBaseUrl).toHaveBeenCalledWith(`${protocol}://${host}${path}`);
            expect(mockHttplease.withTimeout).toHaveBeenCalledWith(DOCKER_DATA_TIMEOUT_MS);
            expect(mockHttplease.withHeaders).toHaveBeenCalledWith(callHeaders);
            expect(mockHttplease.withExpectStatus).toHaveBeenCalledWith([200, 401, 404]);
            expect(mockHttplease.withBufferJsonResponseHandler).toHaveBeenCalledWith({
                maxSize: DOCKER_DATA_MAXSIZE
            });
            expect(mockHttplease.send).toHaveBeenCalled();
        });
    });
});
