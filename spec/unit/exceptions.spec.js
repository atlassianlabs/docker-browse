'use strict';

const handleException = require('../../lib/exceptions').handleException;
const FatalException = require('../../lib/exceptions').FatalException;
const jasminePromiseTools = require('jasmine-promise-tools');
const willResolve = jasminePromiseTools.willResolve;

describe('exceptions', () => {

    describe('handleError', () => {

        beforeEach(() => {

            // capture console.error
            spyOn(console, 'error');
        });

        it('does nothing on success', willResolve(async () => {
            await handleException(Promise.resolve(true));

            expect(console.error).toHaveBeenCalledTimes(0);
            expect(process.exitCode).toBeUndefined();
        }));

        it('prints unexpected error', willResolve(async () => {
            await handleException(Promise.reject(new Error('some error')));

            expect(console.error).toHaveBeenCalledWith('UNHANDLED:');
            expect(console.error).toHaveBeenCalledTimes(2);
            expect(process.exitCode).toEqual(2);
        }));

        it('prints complete FatalException', willResolve(async () => {
            await handleException(Promise.reject(new FatalException(123, 'errorString', 'actionString')));

            expect(console.error).toHaveBeenCalledWith('FATAL:  errorString');
            expect(console.error).toHaveBeenCalledWith('ACTION: actionString');
            expect(console.error).toHaveBeenCalledTimes(2);
            expect(process.exitCode).toEqual(123);
        }));

        it('prints RC-less FatalException', willResolve(async () => {
            await handleException(Promise.reject(new FatalException(undefined, 'errorString', 'actionString')));

            expect(console.error).toHaveBeenCalledWith('FATAL:  errorString');
            expect(console.error).toHaveBeenCalledWith('ACTION: actionString');
            expect(console.error).toHaveBeenCalledTimes(2);
            expect(process.exitCode).toEqual(3);
        }));

        it('prints action-less FatalException', willResolve(async () => {
            await handleException(Promise.reject(new FatalException(456, 'anotherErrorString')));

            expect(console.error).toHaveBeenCalledWith('FATAL:  anotherErrorString');
            expect(console.error).toHaveBeenCalledTimes(1);
            expect(process.exitCode).toEqual(456);
        }));

        afterEach(() => {

            // reset rc
            process.exitCode = undefined;
        });
    });
});