'use strict';

const fs = require('fs');
const path = require('path');
const q = require('q');

const requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();
const jasminePromiseTools = require('jasmine-promise-tools');
const willResolve = jasminePromiseTools.willResolve;

describe('config', () => {
    let config;
    let tmpDirPath;
    let configFilePath;

    beforeEach(willResolve(async () => {

        // mock out requires and inject
        const mockOs = jasmine.createSpyObj(['homedir']);
        config = requireWithMocks('../../lib/config', {
            'os': mockOs
        });

        // create tmpDir
        tmpDirPath = await q.nfcall(fs.mkdtemp, '/tmp' + path.sep);

        // mock out os.homedir to return tmpDir
        mockOs.homedir.and.returnValue(tmpDirPath);

        // expected .docker-browse.json file
        configFilePath = tmpDirPath + path.sep + '.docker-browse.json';
    }));

    describe('read', () => {

        describe('valid .docker-browse.json', () => {

            beforeEach(willResolve(async () => {

                // create valid .docker-browse.json in tmpDir
                await q.nfcall(fs.writeFile, configFilePath, JSON.stringify({
                    defRegistry: 'some.fancy.reg',
                    defProtocol: 'some prot'
                }));
            }));

            it('should return valid config', willResolve(() => {
                return config.read()
                    .then((userConfig) => {
                        expect(userConfig instanceof config.UserConfig).toBeTruthy('expected UserConfig');
                        expect(userConfig.defRegistry).toEqual('some.fancy.reg');
                        expect(userConfig.defProtocol).toEqual('some prot');
                    });
            }));

            afterEach(willResolve(async () => {

                // remove .docker-browse.json
                await q.nfcall(fs.unlink, configFilePath);
            }));
        });

        describe('missing .docker-browse.json', () => {

            it('should return default config', willResolve(() => {
                return config.read()
                    .then((userConfig) => {
                        expect(userConfig instanceof config.UserConfig).toBeTruthy('expected UserConfig');
                        expect(userConfig.defRegistry).toEqual('index.docker.io');
                    });
            }));
        });

        describe('invalid .docker-browse.json', () => {

            beforeEach(willResolve(async () => {

                // create rubbish .docker-browse.json in tmpDir
                await q.nfcall(fs.writeFile, configFilePath, 'some rubbish');

                // capture console.warn
                spyOn(console, 'warn');
            }));

            it('should return default config', willResolve(() => {
                return config.read()
                    .then((userConfig) => {
                        expect(userConfig instanceof config.UserConfig).toBeTruthy('expected UserConfig');
                        expect(userConfig.defRegistry).toEqual('index.docker.io');
                    });
            }));

            afterEach(willResolve(async () => {

                // remove .docker-browse.json
                await q.nfcall(fs.unlink, configFilePath);

                expect(console.warn).toHaveBeenCalledWith(`WARN: invalid ${configFilePath}, ignoring`);
            }));
        });
    });

    describe('write', () => {

        describe('valid .docker-browse.json', () => {

            it('should be written', willResolve(() => {
                const userConfig = new config.UserConfig({
                    defRegistry: 'some.other.registry',
                    defProtocol: 'some protocol'
                });
                return config.write(userConfig)
                    .then(() => {
                        return q.nfcall(fs.readFile, configFilePath)
                            .then((data) => {
                                expect(JSON.parse(data).defRegistry).toEqual('some.other.registry');
                                expect(JSON.parse(data).defProtocol).toEqual('some protocol');
                            });
                    });
            }));

            afterEach(willResolve(async () => {

                // remove .docker-browse.json
                await q.nfcall(fs.unlink, configFilePath);
            }));
        });
    });

    afterEach(willResolve(async () => {

        // remove tmpDir
        await q.nfcall(fs.rmdir, tmpDirPath);
    }));
});