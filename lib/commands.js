'use strict';

const registry = require('./registry');
const config = require('./config');
const handshake = require('./handshake');

/**
 * Retrieve and print all images in a repository, by querying /v2/_catalog against commander.registry
 * 
 * @param {commander.Command} commander
 */
async function images(commander) {
    const path = '/v2/_catalog';

    // retrieve a bearer token for the data
    const bearerToken = await handshake.handshake(commander.protocol, commander.registry, path);

    // retrieve and print the data
    return await registry.printData(commander.protocol, commander.registry, path, bearerToken, (data) => {
        if (data['repositories']) {
            data['repositories'].forEach((image) => console.log(image));
        }
    });
}

/**
 * Populate userConfig with current settings and write to disk.
 * 
 * @param {commander.Command} commander 
 * @param {config.UserConfig} userConfig 
 */
async function save(commander, userConfig) {
    userConfig.defRegistry = commander.registry;

    return await config.write(userConfig);
}

/**
 * Retrieve and print all tags for an image, by querying /v2/commander.args[0]/tags/list against commander.registry
 * 
 * @param {commander.Command} commander 
 */
async function tags(commander) {
    const path = '/v2/' + commander.args[0] + '/tags/list';

    // retrieve a bearer token for the data
    const bearerToken = await handshake.handshake(commander.protocol, commander.registry, path);

    // retrieve and print the data
    return await registry.printData(commander.protocol, commander.registry, path, bearerToken, (data) => {
        if (data['tags']) {
            data['tags'].forEach((tag) => console.log(tag));
        }
    });
}

module.exports = {
    images,
    save,
    tags
};
