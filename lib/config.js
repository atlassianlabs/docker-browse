'use strict';

const q = require('q');

const fs = require('fs');
const os = require('os');
const path = require('path');

/**
 * User configuration
 */
class UserConfig {

    /**
     * @param {object=} raw
     */
    constructor(raw) {
        this.defRegistry = raw && raw.defRegistry || 'index.docker.io';
        this.defProtocol = raw && raw.defProtocol || 'https';
    }
}

/**
 * Read user config from ~/.docker-browse.json
 * 
 * @return {Promise.<UserConfig>} values from file or default
 */
function read() {
    const configFilePath = path.join(os.homedir(), '.docker-browse.json');
    return q.nfcall(fs.readFile, configFilePath)
        .then((data) => {
            return new UserConfig(JSON.parse(data));
        })
        .catch((err) => {
            // warn about any failure other than file not found
            if (err.code !== 'ENOENT') {
                console.warn(`WARN: invalid ${configFilePath}, ignoring`);
            }
            return new UserConfig();
        });
}

/**
 * Write user config to ~/.docker-browse.json
 * 
 * @param {UserConfig} userConfig 
 */
function write(userConfig) {
    const configFilePath = path.join(os.homedir(), '.docker-browse.json');
    return q.nfcall(fs.writeFile, configFilePath, JSON.stringify(userConfig, null, 4));
}

module.exports = {
    UserConfig,
    read,
    write
};