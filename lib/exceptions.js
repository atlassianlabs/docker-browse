'use strict';

class FatalException extends Error {

    /**
     * User readable fatal exception, providing a process return code, message and optional action
     *
     * @param {int} rc
     * @param {string} message
     * @param {string=} action
     */
    constructor(rc, message, action) {
        super(message);
        this.name = this.constructor.name;
        this.rc = rc;
        this.action = action;
    }
}

/**
 * Catch any exceptions emmanating from promise and pretty print, setting an appropriate process return code.
 * 
 * @param {Promise} promise 
 */
function handleException(promise) {
    promise.catch((err) => {
        if (err instanceof FatalException) {
            process.exitCode = err.rc || 3;
            console.error('FATAL:  ' + err.message);
            if (err.action) {
                console.error('ACTION: ' + err.action);
            }
        } else {
            console.error('UNHANDLED:');
            console.error(err);
            process.exitCode = 2;
        }
    });
}

module.exports = {
    FatalException,
    handleException
};
