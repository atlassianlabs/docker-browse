'use strict';

const FatalException = require('./exceptions').FatalException;

const fs = require('fs');
const os = require('os');
const path = require('path');
const q = require('q');

const ACTION_LOGIN_PREFIX = 'docker login ';

/**
 * Read the basic 'auth' key for the host out of 'auths' from ~/.docker/config.json
 *
 * @param {string} host
 * @return {Promise.<string>}
 * @throws {FatalException} rc=13 ~/.docker/config.json missing
 * @throws {FatalException} rc=14 ~/.docker/config.json unreadable
 * @throws {FatalException} rc=15 auths.<host>.auth not present
 * @throws {FatalException} rc=16 docker using OS X keychain to store credentials
 */
function basicAuth(host) {

    const configFilePath = path.join(os.homedir(), '.docker', 'config.json');

    return q.nfcall(fs.readFile, configFilePath)
        .then((data) => {
            const dockerConfig = JSON.parse(data);
            if (dockerConfig['credsStore'] === 'osxkeychain') {
                throw new FatalException(16, configFilePath + ' stores credentials in OS X keychain',
                    'go to docker preferences and untick "Securely store docker logins in macOS keychain"');
            }
            return dockerConfig['auths'][host]['auth'];
        })
        .catch((err) => {
            const actionLogin = ACTION_LOGIN_PREFIX + host;
            if (err.code === 'ENOENT') {
                throw new FatalException(13, 'cannot find ' + configFilePath, actionLogin);
            } else if (err instanceof SyntaxError) {
                throw new FatalException(14, 'cannot parse ' + configFilePath, actionLogin);
            } else if (err instanceof TypeError) {
                throw new FatalException(15, 'cannot find auth for ' + host + ' in auths in ' + configFilePath,
                    actionLogin);
            }
            throw err;
        });
}

module.exports = {
    basicAuth
};
