'use strict';

const FatalException = require('./exceptions').FatalException;

const httplease = require('httplease');
const parseHttpHeader = require('parse-http-header');
const httpLinkHeader = require('http-link-header');

const DOCKER_HANDSHAKE_TIMEOUT_MS = 5000; // 5sec
const DOCKER_DATA_TIMEOUT_MS = 60000; // 1min
const DOCKER_DATA_MAXSIZE = 1 * 1024 * 1024; // 1MiB, up from 512KiB default

function requiresAuth(protocol, host) {
    const baseUrl = `${protocol}://${host}/v2/`;

    return httplease.builder()
        .withMethodGet()
        .withBaseUrl(baseUrl)
        .withTimeout(DOCKER_HANDSHAKE_TIMEOUT_MS)
        .withExpectStatus([200])
        .withDiscardBodyResponseHandler()
        .send()
        .then(() => {
            return false;
        })
        .catch((err) => {
            if (err.code === 'ENOTFOUND') {
                throw new FatalException(22, 'host ' + err.hostname + ' not found');
            } else {
                return true;
            }
        });
}

/**
 * Handshake with ${protocol}://${host}/v2 by performing an unauthenticated GET, expecting a 401 with 'www-authenticate'
 * header.
 *
 * Extract and return bearerRealm and service from the header.
 *
 * @param {string} protocol
 * @param {string} host
 * @return {Promise.<{bearerRealm, service}>}
 * @example handshakeForApiCheck('docker.atlassian.io')
 * @throws {FatalException} rc=21 'www-authenticate' header not returned
 * @throws {FatalException} rc=22 host not found
 * @throws {FatalException} rc=23 'Bearer realm' not present in 'www-authenticate'
 * @throws {FatalException} rc=24 'service' not present in 'www-authenticate'
 * @throws {FatalException} rc=25 on 404 i.e. not a valid V2 registry
 */
function handshakeForApiCheck(protocol, host) {
    const baseUrl = `${protocol}://${host}/v2/`;

    return httplease.builder()
        .withMethodGet()
        .withBaseUrl(baseUrl)
        .withTimeout(DOCKER_HANDSHAKE_TIMEOUT_MS)
        .withExpectStatus([401, 404])
        .withDiscardBodyResponseHandler()
        .send()
        .then((response) => {
            if (response.statusCode === 404) {
                throw new FatalException(25, host + ' does not appear to be a valid V2 API docker registry');
            }

            if (!response.headers['www-authenticate']) {
                throw new FatalException(21, 'www-authenticate header not returned from ' + host);
            }

            // decode the www-authenticate response
            const decoded = parseHttpHeader(response.headers['www-authenticate']);
            if (!decoded['Bearer realm']) {
                throw new FatalException(23, '"Bearer realm" not present in www-authenticate header');
            }
            const bearerRealm = decoded['Bearer realm'].replace(/^"(.*)"$/, '$1');
            if (!decoded['service']) {
                throw new FatalException(24, '"service" not present in www-authenticate header');
            }
            const service = decoded['service'].replace(/^"(.*)"$/, '$1');

            return { bearerRealm, service };
        });
}

/**
 * Retrieve a bearer token from bearerRealm for service, with basic auth using basicAuth
 *
 * @param {string} host - used only for error messages
 * @param {{bearerRealm, service}} handshakeHeader
 * @param {string} basicAuth
 * @returns {Promise.<string>}
 * @throws {FatalException} rc=31 not a V2 API docker registry
 * @throws {FatalException} rc=32 authentication incorrect
 * @throws {FatalException} rc=33 bearer token not returned
 */
function authenticateForApiCheck(host, handshakeHeader, basicAuth) {
    return httplease.builder()
        .withMethodGet()
        .withBaseUrl(handshakeHeader.bearerRealm)
        .withTimeout(DOCKER_HANDSHAKE_TIMEOUT_MS)
        .withHeaders({
            Authorization: 'Basic ' + basicAuth
        })
        .withParams({
            service: handshakeHeader.service
        })
        .withExpectStatus([200, 401, 404])
        .withBufferJsonResponseHandler()
        .send()
        .then((response) => {
            if (response.statusCode === 404) {
                throw new FatalException(31, host + ' does not appear to be a valid V2 API docker registry');
            }
            if (response.statusCode === 401) {
                throw new FatalException(32, 'failed authentication', 'docker login ' + host);
            }
            if (!response.body['token']) {
                throw new FatalException(33, 'token not returned from ' + handshakeHeader.bearerRealm);
            }
            return response.body['token'];
        });
}

/**
 * Ensure that the registry is V2 HTTP API capable, by performing an authorised get against https://${host}/v2
 * A response of 200 indicates capability and will return nothing.
 *
 * @param {string} protocol
 * @param {string} host
 * @param {string} bearerToken
 * @returns {Promise<null>}
 * @throws {FatalException} rc=41 on 404
 */
function apiCheck(protocol, host, bearerToken) {
    const baseUrl = `${protocol}://${host}/v2/`;

    return httplease.builder()
        .withMethodGet()
        .withBaseUrl(baseUrl)
        .withTimeout(DOCKER_HANDSHAKE_TIMEOUT_MS)
        .withHeaders({
            Authorization: 'Bearer ' + bearerToken
        })
        .withExpectStatus([200, 404])
        .withDiscardBodyResponseHandler()
        .send()
        .then((response) => {
            if (response.statusCode === 404) {
                throw new FatalException(41, host + ' does not appear to be a valid V2 API docker registry');
            }
        });
}


/**
 * Handshake with ${protocol}://${host}${path} by performing an unauthenticated GET, expecting a 401 with
 * 'www-authenticate' header.
 *
 * Extract and return bearerRealm, service and scope from the header.
 *
 * @param {string} protocol
 * @param {string} host
 * @param {string} path
 * @returns {Promise.<{bearerRealm, service, scope}>}
 * @throws {FatalException} rc=51 'www-authenticate' header not returned
 * @throws {FatalException} rc=52 ${host}${path} returns 404
 * @throws {FatalException} rc=53 'Bearer realm' not present in 'www-authenticate'
 * @throws {FatalException} rc=54 'service' not present in 'www-authenticate'
 * @throws {FatalException} rc=55 'scope' not present in 'www-authenticate'
 */
function handshakeForData(protocol, host, path) {
    const baseUrl = `${protocol}://${host}${path}`;

    return httplease.builder()
        .withMethodGet()
        .withBaseUrl(baseUrl)
        .withTimeout(DOCKER_HANDSHAKE_TIMEOUT_MS)
        .withExpectStatus([401, 404])
        .withDiscardBodyResponseHandler()
        .send()
        .then((response) => {
            if (response.statusCode === 404) {
                throw new FatalException(52, `https://${host}${path} not found`);
            }
            if (!response.headers['www-authenticate']) {
                throw new FatalException(51, 'www-authenticate header not returned from ' + host);
            }

            // decode the www-authenticate response
            const decoded = parseHttpHeader(response.headers['www-authenticate']);
            if (!decoded['Bearer realm']) {
                throw new FatalException(53, '"Bearer realm" not present in www-authenticate header');
            }
            const bearerRealm = decoded['Bearer realm'].replace(/^"(.*)"$/, '$1');
            if (!decoded['service']) {
                throw new FatalException(54, '"service" not present in www-authenticate header');
            }
            const service = decoded['service'].replace(/^"(.*)"$/, '$1');
            if (!decoded['scope']) {
                throw new FatalException(55, '"scope" not present in www-authenticate header');
            }
            const scope = decoded['scope'].replace(/^"(.*)"$/, '$1');

            return { bearerRealm, service, scope };
        });
}

/**
 * Retrieve a bearer token from bearerRealm for service/scope, with basic auth using basicAuth
 *
 * @param {{bearerRealm, service, scope}} authHeader
 * @param {string} basicAuth
 * @return {Promise.<string>}
 * @throws {FatalException} rc=61 when 401 returned
 * @throws {FatalException} rc=62 when no token returned
 */
function authenticateForData(authHeader, basicAuth) {
    return httplease.builder()
        .withMethodGet()
        .withBaseUrl(authHeader.bearerRealm)
        .withTimeout(DOCKER_HANDSHAKE_TIMEOUT_MS)
        .withHeaders({
            Authorization: 'Basic ' + basicAuth
        })
        .withParams({
            service: authHeader.service,
            scope: authHeader.scope
        })
        .withExpectStatus([200, 401])
        .withBufferJsonResponseHandler()
        .send()
        .then((response) => {
            if (response.statusCode === 401) {
                throw new FatalException(41, 'received data auth response from ' + authHeader.bearerRealm +
                    ' "' + response.body['details'] + '"');
            } else if (!response.body['token']) {
                throw new FatalException(42, 'token not found in response from ' + authHeader.bearerRealm);
            }
            return response.body['token'];
        });
}

/**
 * Perform a GET against ${protocol}://${host}${path} and print the results via printFn.
 * Bearer authentication will be used if provided.
 * Will recursively invoke until response is 404 or does not contain 'link' header with a valid rel="next" link.
 *
 * @param {string} protocol
 * @param {string} host
 * @param {string} path
 * @param {string} bearerToken may be null, to indicate no security headers required
 * @param {function} printFn
 * @throws {FatalException} rc=71 when 401 returned
 */
function printData(protocol, host, path, bearerToken, printFn) {
    const baseUrl = `${protocol}://${host}${path}`;

    const headers = bearerToken ? { Authorization: 'Bearer ' + bearerToken } : {};

    return httplease.builder()
        .withMethodGet()
        .withBaseUrl(baseUrl)
        .withTimeout(DOCKER_DATA_TIMEOUT_MS)
        .withHeaders(headers)
        .withExpectStatus([200, 401, 404])
        .withBufferJsonResponseHandler({maxSize: DOCKER_DATA_MAXSIZE})
        .send()
        .then((response) => {
            if (response.statusCode === 401) {
                // unauthorised at this point indicates a fine-grained access issue
                throw new FatalException(71, 'not authorised for ' + path);
            }
            if (response.statusCode === 200) {
                // print and paginate the data
                printFn(response.body);

                // retrieve and query the rfc5988 rel="next" link
                const linkHeader = response.headers['link'];
                if (linkHeader) {
                    const nextLink = httpLinkHeader.parse(linkHeader).get('rel', 'next');
                    if (nextLink && nextLink.length > 0 && nextLink[0].uri) {
                        return printData(protocol, host, nextLink[0].uri, bearerToken, printFn);
                    }
                }
            }
            // 404 indicates no information, so do nothing
        });
}

module.exports = {
    requiresAuth,
    handshakeForApiCheck,
    authenticateForApiCheck,
    apiCheck,
    handshakeForData,
    authenticateForData,
    printData
};
