'use strict';

const dockerAuth = require('./dockerAuth');
const registry = require('./registry');

/**
 * Authenticate and authorise with a V2 API registry, returning an optional bearer token.
 * 
 * @param {string} protocol
 * @param {string} host
 * @param {string} path
 * @return {Promise.<string>} bearer token or null if no auth required
 */
async function handshake(protocol, host, path) {

    // can we access this repository anonymously?
    if (!await registry.requiresAuth(protocol, host)) {
        return null;
    }

    // handshake with the registry to check V2 API
    const apiCheckHeader = await registry.handshakeForApiCheck(protocol, host);

    // read the user's basic auth
    const dockerBasicAuth = await dockerAuth.basicAuth(host);

    // authenticate with the registry
    const apiCheckToken = await registry.authenticateForApiCheck(host, apiCheckHeader, dockerBasicAuth);

    // ensure that the registry is V2 capable
    await registry.apiCheck(protocol, host, apiCheckToken);

    // handshake for the requested data
    const dataHeader = await registry.handshakeForData(protocol, host, path);

    // authenticate for the data
    return await registry.authenticateForData(dataHeader, dockerBasicAuth);
}

module.exports = {
    handshake
};
